package de.obdev.network.session;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.io.BaseEncoding;
import com.google.common.primitives.Longs;
import de.obdev.crypto.symmetric.SymmetricCrypt;
import de.obdev.crypto.symmetric.SymmetricKey;
import de.obdev.util.random.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

public class Sessions {

    static Cache<Thread, Session> SessionsCache = CacheBuilder.newBuilder()
            .expireAfterAccess(1, TimeUnit.MINUTES)
            .build();
    static Cache<Session, byte[]> imageCache = CacheBuilder.newBuilder()
            .expireAfterAccess(1, TimeUnit.MINUTES)
            .build();

    static Map<Long, SymmetricCrypt> crypts = new HashMap<>();

    static Callable<Session> creator;

    static SymmetricCrypt crypt;

    static CountDownLatch latch = new CountDownLatch(1);

    static Boolean warned = false;

    static Logger logger = LogManager.getLogger();

    static Checksum checksum = new Adler32();

    public static void addCrypt(SymmetricCrypt crypt) throws Exception {
        byte[] base = crypt.encrypt("Session".getBytes("UTF-8"));

        checksum.update(
                base, 0, base.length
        );
        Long sum = checksum.getValue();
        crypts.put(sum, crypt);
        logger.info("Added crypt provider ({}) as {}", crypt.getClass().getName(), sum);
        latch.countDown();
    }

    public static long getCrypt() {
        return new ArrayList<>(crypts.keySet()).get(Random.nextInt(0, crypts.size() -1));
    }

    public static void setCreator(Callable<Session> creator) {
        Sessions.creator = creator;
    }

    public static void await() {
        if (!warned && latch.getCount() > 0) {
            logger.fatal("Crypt is not set, blocking util change!");
            warned = true;
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void setCurrent(String base64) {
        Session current = unpack(base64);
        setCurrent(current);
    }

    public static void setCurrent(Session current) {
        SessionsCache.put(Thread.currentThread(), current);
        imageCache.put(current, serialize(current));
    }

    public static Session getCurrent() {
        try {
            Session current = SessionsCache.get(Thread.currentThread(), () -> {
                if (creator == null) throw new SessionsNotFoundException("Unable to find current Sessions");
                Session newSession = creator.call();
                setCurrent(newSession);
                return newSession;
            });
            return current;
        } catch (Exception e) {
            throw new SessionsAccessException("Unable to access Sessions cache", e);
        }
    }

    public static Boolean changed() {
        Session current = getCurrent();
        byte[] now = serialize(current);
        try {
            byte[] start = imageCache.get(current, () -> now);
            return !Arrays.equals(now, start);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }


    static byte[] serialize(Object object) {
        if (object instanceof Serializable) {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            try {
                ObjectOutputStream oos = new ObjectOutputStream(bout);
                oos.writeObject(object);
                return bout.toByteArray();
            } catch (IOException e) {
                throw new SessionsConvertException(
                        "Unable to serialize current Sessions", e
                );
            }
        }
        return null;
    }

    private static Session deserialize(byte[] bytes) {
        try {
            ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(bytes));
            Object object = oin.readObject();
            if (object instanceof Session) {
                return (Session) object;
            } else {
                throw new SessionsConvertException("Unable to deserialize as Sessions");
            }
        } catch (Exception e) {
            throw new SessionsConvertException("Unable to deserialize as Sessions", e);
        }
    }

    protected static String pack(Session session) {
        await();
        byte[] data = serialize(session);
        crypt = crypts.get(session.crypt);
        if (crypt == null) throw new SessionsConvertException("Unable to find crypt with id " + session.crypt);
        try {
            byte[] enc = crypt.encrypt(data);
            ByteBuffer buffer = ByteBuffer.allocate(enc.length + 8);
            buffer.putLong(checksum.getValue());
            buffer.put(enc);
            buffer.position(0);
            byte[] result = new byte[buffer.remaining()];
            buffer.get(result);
            return BaseEncoding.base64().encode(result);
        } catch (Exception e) {
            throw new SessionsConvertException("Unable to pack session", e);
        }
    }

    protected static Session unpack(String s) {
        await();

        byte[] decoded = BaseEncoding.base64().decode(s);
        ByteBuffer buffer = ByteBuffer.wrap(decoded);
        long cryptId = buffer.getLong();
        byte[] result = new byte[buffer.remaining()];
        buffer.get(result);

        SymmetricCrypt crypt = crypts.get(cryptId);
        if (crypt == null) throw new SessionsConvertException("Unable to find crypt with id " + cryptId);
        try {
            byte[] dec = crypt.decrypt(result);
            return deserialize(dec);
        } catch (Exception e) {
            throw new SessionsConvertException("Unable to decrypt Sessions", e);
        }
    }

    static class SessionsAccessException extends RuntimeException {
        public SessionsAccessException(String message) {
            super(message);
        }

        public SessionsAccessException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    static class SessionsInitException extends RuntimeException {
        public SessionsInitException(String message) {
            super(message);
        }

        public SessionsInitException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    static class SessionsNotFoundException extends RuntimeException {
        public SessionsNotFoundException(String message) {
            super(message);
        }

        public SessionsNotFoundException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    static class SessionsConvertException extends RuntimeException {
        public SessionsConvertException(String message) {
            super(message);
        }

        public SessionsConvertException(String message, Throwable cause) {
            super(message, cause);
        }
    }


}
