package de.obdev.network.session;

import de.obdev.util.random.Random;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;

public class Session implements Serializable {


    byte[] rand;
    byte[] ip;
    long crypt;

    public Session(String host, long crypt) throws IOException {
        this.rand = Random.nextBytes(32);
        this.ip = InetAddress.getByName(host).getAddress();
        this.crypt = crypt;
    }

    @Override
    public String toString() {
        return Sessions.pack(this);
    }

}
